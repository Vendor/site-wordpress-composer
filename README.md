# WordPress Composer Site Skeleton

## Install WordPress latest nightly build

	composer install

## Install latest stable release of WordPress Version 3.8.1

	COMPOSER=composer-stable.json composer install

## Enjoy
